﻿using ChargingSystemPortalDashboard.Data;
using ChargingSystemPortalLocalization.Data;
using ChargingSystemPortalLocalization.Logging;
using LoggerService;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



namespace ChargingSystemPortalLocalization.Controllers
{
    [Route("api/")]
    [ApiController]
    public class ChargingSystemPortalLocalization : ControllerBase
    {
        // Repository var definition
        private readonly IDataRepository _dataRepository;

        // Controller constractor - init local injected vars
        public ChargingSystemPortalLocalization(IDataRepository dataRepository, IConfiguration configuration)
        {
            // Repository injection
            _dataRepository = dataRepository;

        }

        // Error routing handler for develop mode
        [Route("/error-local-development")]
        public IActionResult ErrorLocalDevelopment(
                 [FromServices] IWebHostEnvironment webHostEnvironment)
        {
            // Check if in dev environmen mode
            if (webHostEnvironment.EnvironmentName != "Development")
            {
                throw new InvalidOperationException(
                    "This shouldn't be invoked in non-development environments.");
            }

            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();

            // Return formated exception content
            return Problem(
                detail: context.Error.StackTrace,
                title: context.Error.Message);
        }

        // Error routing handler for production mode
        [Route("/error")]
        public IActionResult Error() => Problem();
        
        
        // GET: api/<PortalLocalizationController>
        [HttpGet("localization/")]
        public async Task<ActionResult<IEnumerable<LocalizationInfo>>> GetLocalization()
        {
            try
            {
                // Set user identity
                SetUserIdentity();

                // Get all localizatio info
                var localization = await _dataRepository.GetAllLocalizationInfo();

                // Returnd value check
                return (localization.Count() == 0) ? NotFound() : Ok(localization);
            }
            catch (Exception ex)
            {
                // Adding log relevalt exception info +
                // Throw exception to upper handling level(error middleware)
                throw new InvalidOperationException("Error on GetLocalization", GetExceptionLogInfo("GetLocalization", ex));

            }
        }



        [HttpPut("localization/{id}")]
        public ActionResult<LocalizationInfo> PutLocalization(int id, string value = "newVal", string comment = "newComm")
        {
            try
            {
                // Set user identity
                SetUserIdentity();

                // Put 
                var localization = _dataRepository.PutLocalization(id, value, comment);

                // Returnd value check
                return (localization == null) ? NotFound() : Ok(localization);
            }
            catch (Exception ex)
            {
                // Adding log relevalt exception info +
                // Throw exception to upper handling level(error middleware)
                throw new InvalidOperationException("Error on PutLocalization", GetExceptionLogInfo("PutLocalization", ex));

            }

        }

        // DELETE api/<PortalLocalizationController>/5
        [HttpDelete("localization/{id}")]
        public ActionResult Delete(int id)
        {
            // Set user identity
            SetUserIdentity();

            _dataRepository.DeleteLocalization(id);
            return NoContent();

        }

        private void SetUserIdentity()
        {
            //var context = HttpContext.Session .Features.Get<IExceptionHandlerFeature>();
            if (string.IsNullOrEmpty(HttpContext.Session.GetString("UserKey")))
            {
               // var _actionInfo = new ActionLogInformation();
                HttpContext.Session.SetString("UserKey", HttpContext.Session.Id);
               // _actionInfo.SetUserID(HttpContext.Session.GetString("UserKey"));
            }


        }

        private Exception GetExceptionLogInfo(string actionName, Exception actionException)
        {
            Dictionary<string, ActionLogInfo> _parameters = new ActionLogInformation().GetLogDictonary();
            string actionKey = actionName + " (ChargingSystemPortalDashboard)";

             actionException.Data.Add("LogInfo", new LogInformation
            {
                UserID = (int)PortalUserID.UserID,
                ServiceType = _parameters[actionKey].ServiceType,
                LevelEx = (int)LogLevel.Error,
                Msg = _parameters[actionKey].ActionErrorMsg + actionException.Message,
                Created = DateTime.Now
            });

            return actionException;
         
        }
    }
}
