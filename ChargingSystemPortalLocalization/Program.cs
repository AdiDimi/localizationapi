using LoggerService;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChargingSystemPortalLocalization
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var logger = new LoggerManager();
            try
            {
                // logger.Debug("init main");
                CreateWebHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                // NLog: catch setup errors
                logger.LogError(ex, "Stopped program because of exception");
                throw;
            }
            finally
            {
                // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                logger.Flush();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
     WebHost.CreateDefaultBuilder(args)
          // .AddEnvironmentVariables();
          .UseStartup<Startup>()
             .UseUrls("http://cellocharge.com/", "https://cellocharge.com/")
         .ConfigureAppConfiguration(AddAppConfiguration)
        
         .ConfigureLogging(logging =>
         {
            logging.ClearProviders();
            logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
        });

        private static void AddAppConfiguration(WebHostBuilderContext hostingContext, IConfigurationBuilder config)
        {
            //throw new NotImplementedException();
            config.AddEnvironmentVariables();
        }
    }
}
