﻿//using ChargingSystemPortal.Data.Models;
using ChargingSystemPortalDashboard.Data;
using Dapper;
using DBHelper.Data;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;


namespace ChargingSystemPortalLocalization.Data
{
    // DataRepository - this class centralize all DBHelper methodes and uses base class IDataRepository to handle db/dapper specification commands   
    public class DataRepository : BaseRepository, IDataRepository
    {

                                                                 
        private const string SELECT_All_LOCALIZATION_INFO = "SELECT  [Id] ,[SystemID] ,[resourceType] ,IIF([cultureCode]='1','ENG',IIF([cultureCode]='2','HEB',IIF([cultureCode]='3','ARAB','RUS'))) as cultureCode  ,[resourceValue] ,[Comment] " +
                                                            "FROM[ChargingSystem].[dbo].[Localization]";

        private const string SELECT_LOCALIZATION_BY_ID = "SELECT  [Id] ,[SystemID] ,[resourceType] ,[cultureCode] ,[resourceValue] ,[Comment] " +
                                                         "FROM[ChargingSystem].[dbo].[Localization] " +
                                                         "WHERE Id= @id";



        private const string PUT_PROVIDER_LOCALIZATION_INFO = "UPDATE Localization " +
                                                               "SET Localization.resourceValue = @providerName " +
                                                               "WHERE Localization.Id IN(" +
                                                                                    "SELECT Localization.Id AS localID " +
                                                                                    "FROM   StationLocalization INNER JOIN " +
                                                                                                     "StationProvider INNER JOIN " +
                                                                                                     "Station ON StationProvider.ID = Station.ProviderID ON StationLocalization.StationID = Station.ID INNER JOIN " +
                                                                                                     "Localization ON StationLocalization.LocalizationID = Localization.Id " +
                                                                                    "WHERE   (Localization.cultureCode = '2') AND(Localization.resourceType = 'Station_ProviderName') AND(StationProvider.ID = @providerID))";

        private const string PUT_STATION_NAME_LOCALIZATION_INFO = "UPDATE Localization " +
                                                               "SET Localization.resourceValue = @stationName " +
                                                               "WHERE Localization.Id IN(" +
                                                                                    "SELECT Localization.Id AS localID " +
                                                                                    "FROM   StationLocalization INNER JOIN " +
                                                                                                      "Station ON StationLocalization.StationID = Station.ID INNER JOIN " +
                                                                                                     "Localization ON StationLocalization.LocalizationID = Localization.Id " +
                                                                                    "WHERE   (Localization.cultureCode = '2') AND(Localization.resourceType = 'Station_Name') AND(Station.ID = @stationID)) ";

        private const string PUT_STATION_ADDRESS_LOCALIZATION_INFO = "UPDATE Localization " +
                                                              "SET Localization.resourceValue = @address " +
                                                              "WHERE Localization.Id IN(" +
                                                                                   "SELECT Localization.Id AS localID " +
                                                                                   "FROM   StationLocalization INNER JOIN " +
                                                                                                     "Station ON StationLocalization.StationID = Station.ID INNER JOIN " +
                                                                                                    "Localization ON StationLocalization.LocalizationID = Localization.Id " +
                                                                                   "WHERE   (Localization.cultureCode = '2') AND(Localization.resourceType = 'Station_StreetName') AND(Station.ID = @stationID)) ";

        private const string PUT_LOCALIZATION_INFO = "UPDATE [ChargingSystem].[dbo].[Localization] " +
                                                             "SET [Comment] = @comment ,[resourceValue] = @updateValue " +
                                                             "WHERE Id = @id";
        private const string DELETE_LOCALIZATION_INFO = "DELETE FROM[ChargingSystem].[dbo].[Localization] " +
                                                        "WHERE Id = @id";

      

        // DataRepository constractor - Sends configuration requerments to base class
        public DataRepository(IConfiguration configuration) : base(configuration) { }


   
        // This method returns a summarised info list of stations and there providers
        public async Task<IEnumerable<LocalizationInfo>> GetAllLocalizationInfo()
        {
    
            return (await AsyncQuery<LocalizationInfo>(SELECT_All_LOCALIZATION_INFO));


        }

   


        public LocalizationInfo GetLocalizationRowByID(int id)
        {
            // SQL dynamic param init
            var parameters = new DynamicParameters();
            parameters.Add(name: "@id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            return QueryFirstOrDefault<LocalizationInfo>(SELECT_LOCALIZATION_BY_ID, parameters);
        }

        public LocalizationInfo PutLocalization(int id, string updateValue, string comment)
        {
            // SQL dynamic param init
            var parameters = new DynamicParameters();
            parameters.Add(name: "@id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            parameters.Add(name: "@comment", comment, dbType: DbType.String, direction: ParameterDirection.Input); //LocaleCode
            parameters.Add(name: "@updateValue", updateValue, dbType: DbType.String, direction: ParameterDirection.Input); //LocaleCode

            var returnId = Execute(PUT_LOCALIZATION_INFO, parameters);
            return GetLocalizationRowByID(id);
        }

   

        public async Task<int> PutProviderLocalization(PutProvider provider)
        {
            // SQL dynamic param init
            var parameters = new DynamicParameters();
            parameters.Add(name: "@providerID", provider.ProviderId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            parameters.Add(name: "@providerName", provider.ProviderName, dbType: DbType.String, direction: ParameterDirection.Input); //LocaleCode

            var returnId = await AsyncQueryFirstOrDefault<int>(PUT_PROVIDER_LOCALIZATION_INFO, parameters);
            return (returnId);
        }

 

        public void DeleteLocalization(int id)
        {
            // SQL dynamic param init
            var parameters = new DynamicParameters();
            parameters.Add(name: "@id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            Excute(DELETE_LOCALIZATION_INFO, parameters);
        }
     

    



    }
}

