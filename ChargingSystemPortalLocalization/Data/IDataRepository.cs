﻿//using ChargingSystemPortal.Data.Models;
using ChargingSystemPortalDashboard.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChargingSystemPortalLocalization.Data
{
    // Base repository interface class
    public interface IDataRepository
    {
         Task<IEnumerable<LocalizationInfo>> GetAllLocalizationInfo();
         LocalizationInfo PutLocalization(int id, string updateValue, string comment);
         LocalizationInfo GetLocalizationRowByID(int id);
         void DeleteLocalization(int id);
          Task<int> PutProviderLocalization(PutProvider provider);
      

    }
}
