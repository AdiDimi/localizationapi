﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ChargingSystemPortalDashboard.Data
{
    public class DayConsumption
    {
        [Required]
        public string ConsumptionDay { get; set; }

        [Required]
        public int Consumption { get; set; }

        [Required]
        public int WeekBeforeConsumption { get; set; }

        [Required]
        public decimal ChangePersent { get; set; }

        public string LastUpdate { get; set; }

    }
}
