﻿using DBHelper.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ChargingSystemPortalDashboard.Data
{
    public class MenuCategoryItem : MenuPageItem
    {
     
        //public int ItemLevel { get; set; }

 
        public MenuCategoryItem(IEnumerable<MenuItem> menuItems, int id, string pageDescription, int pageType, int oderNumber) :base( id, pageDescription, pageType, oderNumber)
        {
           
            foreach(MenuItem itm in menuItems.Where(mnuItm => mnuItm.ID ==this.ID))
                AddChild(new MenuPageItem(itm.childID, itm.childPageDescription,  itm.childPageType,  itm.chlidOrderNumber), menuItems);
        }

        public MenuCategoryItem(IEnumerable<MenuItem> menuItems, MenuPageItem ctgory) : base(ctgory.ID, ctgory.PageDescription, ctgory.PageType, ctgory.OderNumber)
        {
           
            foreach (MenuItem itm in menuItems.Where(mnuItm => mnuItm.ID == this.ID))
                AddChild(new MenuPageItem(itm.childID, itm.childPageDescription, itm.childPageType, itm.chlidOrderNumber), menuItems);
        }

   

        public void AddChild(MenuPageItem childObject, IEnumerable<MenuItem> menuItems)
        {
            
            if ((((MenuPageItem)childObject).PageType == 2))
                Pages.Add((MenuPageItem)childObject);
            else
            {
                Pages.Add(new MenuCategoryItem(menuItems, childObject));


            }

        }

    }
}
