﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ChargingSystemPortalLocalization.Data
{
    public class LocalizationInfo 
    {
        [Required]
        public int Id { get; set; }

        [Column(TypeName = "varchar (200)"), Required]
        public string resourceType { get; set; }
        
        [Column(TypeName = "nvarchar(1000)"), Required]
        public string resourceValue { get; set; }

        [Column(TypeName = "varchar (10)"), Required]
        public string cultureCode { get; set; }

        [Column(TypeName = "varchar (10)"), Required]
        public string SystemID { get; set; }

        [Column(TypeName = "nvarchar(1000)")]
        public string Comment { get; set; }

    }
}
