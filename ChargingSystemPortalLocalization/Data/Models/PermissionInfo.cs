﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ChargingSystemPortalDashboard.Data
{
    public class PermissionInfo
    {
        [Required]
        public int ID { get; set; }

        [Column(TypeName = "varchar (50)"), Required]
        public string Name { get; set; }

        [Column(TypeName = "varchar (50)"), Required]
        public string OpeningPage { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string Source { get; set; }

        [Required]
        public int Users { get; set; }

        [Column(TypeName = "varchar (50)"), Required]
        public string Status { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        public DateTime LastChanged { get; set; }
    }
}
