﻿using DBHelper.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ChargingSystemPortalDashboard.Data
{
    public class MenuItem
    {

        public int ID { get; set; }

        [Column(TypeName = "varchar (50)"), Required]
        public string PageDescription { get; set; }

        public int PageType { get; set; }

        public int OrderNumber { get; set; }

        public int itemLevel { get; set; }

        public int childID { get; set; }

        [Column(TypeName = "varchar (50)"), Required]
        public string childPageDescription { get; set; }


        public int childPageType { get; set; }

        public int chlidOrderNumber { get; set; }



    }
}
