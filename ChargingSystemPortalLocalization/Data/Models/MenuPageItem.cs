﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DBHelper.Data.Models;

namespace ChargingSystemPortalDashboard.Data
{
    public class MenuPageItem 
    {
        public int ID { get; set; }

        [Column(TypeName = "varchar (50)"), Required]
        public string PageDescription { get; set; }

        public int PageType { get; set; }

        public int OderNumber { get; set; }

        public List<MenuPageItem> Pages { get; set; } = new List<MenuPageItem>();

     
        public MenuPageItem(int id, string pageDescription, int pageType, int oderNumber)
        {
            ID = id;
            PageDescription = pageDescription;
            PageType = pageType;
            OderNumber = oderNumber;
        }

    }
}
