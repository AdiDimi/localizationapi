﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChargingSystemPortalDashboard.Data
{
    public class ProviderInformation
    {
        [Required]
        public int ProviderId { get; set; }

        [Column(TypeName = "varchar (20)"), Required]
        public string ProviderName { get; set; }

        [Required]
        public string AvailableStationsCounter { get; set; }
        [Required]
        public int TotalConsumptionTime { get; set; }

        [Required]
        public int TotalChargedVehicles { get; set; }

        [Required]
        public int TotalCO2Saved { get; set; }

        [Required]
        public int AveragePrice { get; set; }

        [Required]
        public int DailyConsumption { get; set; }

        public int ProviderType { get; set; }
       
        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        public bool Active { get; set; }

        public string Address { get; set; }

        public string Contact { get; set; }

        public string PhoneNumber { get; set; }

        public string SettingUp { get; set; }

        public string LastChange { get; set; }

     

    }
}
