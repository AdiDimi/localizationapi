﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ChargingSystemPortalDashboard.Data
{
    public class PutProvider
    {
        [Required]
        public int ProviderId { get; set; }

        [Column(TypeName = "varchar (20)"), Required]
        public string ProviderName { get; set; }

        public string Address { get; set; }

        public string Contact { get; set; }

        public string PhoneNumber { get; set; }

        public string LastChange { get; set; }

        public bool Active { get; set; }


    }
}
