﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChargingSystemPortalDashboard.Data
{
    public class ViewsReport
    {

        [Column(TypeName = "datetime")]
        public DateTime ViewDate { get; set; } = new DateTime();

        [Required]
        public int Type { get; set; }
 
        public string Navigate { get; set; }

        public int ViewTime { get; set; }

        public string Platform { get; set; }

    
    }
}
