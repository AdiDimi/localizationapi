﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChargingSystemPortalDashboard.Data
{
    public class UserInfo
    {
        [Required]
        public int ID { get; set; }

        [Column(TypeName = "varchar (50)"), Required]
        public string Name { get; set; }

        [Column(TypeName = "varchar (50)"), Required]
        public string Departement { get; set; }

        [Column(TypeName = "nvarchar(10)")]
        public string Phone { get; set; }

        [Required]
        public int AccessGroup { get; set; }

        [Column(TypeName = "varchar (50)"), Required]
        public string UserName { get; set; }

        [Column(TypeName = "varchar (50)"), Required]
        public string Pass { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        public DateTime LastLogin { get; set; }



        //  []
        //,[]
        //,[]
        //,[]
        //,[]
        //,[]
        //,[]
        //,[]
        //,[]

    }
}
