﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ChargingSystemPortalDashboard.Data
{
    public class TotalConsumption
    {
        [Required]
        public IEnumerable< HourConsumption> HoursConsumption { get; set; }
        [Required]
        public IEnumerable<DayConsumption> DaysConsumption { get; set; }
        [Required]
        public IEnumerable<MonthConsumption> MonthsConsumption { get; set; }

        public DateTime? LastModified { get; set; } = new DateTime();

    }
}
