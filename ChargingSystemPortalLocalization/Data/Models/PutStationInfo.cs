﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChargingSystemPortalDashboard.Data
{
    public class PutStationInfo
    {
        [Required]
        public int ID { get; set; }

       [Column(TypeName = "varchar (50)"), Required]
        public string Station_Name { get; set; }


        [StringLength(50)]
        public string Address { get; set; }


       // public string SettingUp { get; set; }

       // public string LastChange { get; set; }

    }
}
