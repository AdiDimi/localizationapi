﻿using DBHelper.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChargingSystemPortalDashboard.Data
{
    public class StationInformation 
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public string ProviderName { get; set; }

        [Column(TypeName = "varchar (50)"), Required]
        public string Station_Name { get; set; }

        [Column(TypeName = "money")]
        public decimal AvgPrice { get; set; }

        [StringLength(50)]
        public string Station_StreetName { get; set; }

    
        [Column(TypeName = "varchar (50)"), Required]
        public string ConnectorType { get; set; }

        
        public int Type { get; set; }

     
        [Required]
        public int Status { get; set; }

        public string SettingUp { get; set; }

        public string LastChange { get; set; }




    }
}
