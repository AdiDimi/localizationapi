﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChargingSystemPortalDashboard.Data
{
    public class HourConsumption
    {
        public string ConsumptionTime { get; set; }

        [Required]
        public int Consumption { get; set; }

        [Required]
        public int DayBeforeConsumption { get; set; }

        [Required]
        public decimal ChangePersent { get; set; }

        public string LastUpdate { get; set; }
    }
}
