﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ChargingSystemPortalDashboard.Data
{
    public class TotalConsumptionInfo
    {
        //[Required]
        //public int ProviderId { get; set; }

        [Required]
        public int TotalConsumption { get; set; }

        [Required]
        public int TotalConsumptionTime { get; set; }

        [Required]
        public int TotalChargedVehicles { get; set; }

        [Required]
        public int TotalCO2Saved { get; set; }

    }
}
