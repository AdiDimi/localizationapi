﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
//using Microsoft.EntityFrameworkCore;

#nullable disable

namespace ChargingSystemPortalDashboard.Data
{
    
    public partial class StationProvider
    {
        public StationProvider()
        {
           // Stations = new HashSet<Station>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string ProviderName { get; set; }
        public int ProviderType { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Created { get; set; }

        public bool Active { get; set; }

        //[InverseProperty(nameof(Station.Provider))]
        //public virtual ICollection<Station> Stations { get; set; }
    }
}
