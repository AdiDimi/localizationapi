﻿
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LoggerService
{
    public class AutoLogActionsFilter : IAsyncActionFilter  
    {
        private readonly Dictionary<string, ActionLogInfo> _parameters; 
  
        // Logger var definition
        private ILoggerManager _logger;
        private readonly IActionLogInformation _actionInfo;

        public AutoLogActionsFilter(IActionLogInformation actionInfo)
        {
            _actionInfo = actionInfo;
            _parameters = actionInfo.GetLogDictonary();
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
                         
            string actionName = GetSubString(context.ActionDescriptor.DisplayName, ".");
            string queryParams = GetSubString(context.HttpContext.Request.Path.ToString(), "/");
        
    
            // Execute any code before the action executes
            var result = await next();

            // Execute any code after the action executes

            LogActionInfo(result, actionName, queryParams);
    
          
         }


        private string GetSubString(string sourceStr, string innerStr)
        {
            int start = sourceStr.LastIndexOf(innerStr);
            int end = sourceStr.Length - start;
            return (start > 0) ? sourceStr.Substring(start + 1, end - 1) : "";
        }

        private void LogActionInfo(ActionExecutedContext result, string actionName, string queryParams)
        {
            if (actionName != "" && result.Result != null)
            {
                _logger = new LoggerManager();
                bool hasErrorStatus = (!result.Result.GetType().Name.Contains("Ok"));

                LogInformation logAct = new LogInformation
                {
                    UserID = _actionInfo.GetUserID(),
                    ServiceType = (_parameters[actionName].ActionParams && queryParams != "") ? Convert.ToInt32(queryParams) : _parameters[actionName].ServiceType,
                    LevelEx = hasErrorStatus? (int)LogLevel.Error: (int)LogLevel.Information,
                    Msg = hasErrorStatus ? _parameters[actionName].ActionErrorMsg: _parameters[actionName].PostActionMsg  + queryParams,
                    Created = DateTime.Now
                };
                _logger.LogInformation("{@LogInfo}", logAct);

            }
        }


 
    }
}

