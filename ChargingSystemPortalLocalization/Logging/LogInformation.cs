﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;

namespace LoggerService
{
 

    public interface IActionLogInformation
    {
         Dictionary<string, ActionLogInfo> GetLogDictonary();
         int GetUserID();
    }

    public class ActionLogInfo
    {
        public int ServiceType { get; set; }
        public string PreActionMsg { get; set; }
        public string PostActionMsg { get; set; }
        public string ActionErrorMsg { get; set; }
        public bool ActionParams{ get; set; }
    }

        
    public class LogInformation
    {
    
        public int? UserID { get; set; }
        public int ServiceType { get; set; }
        public int LevelEx { get; set; }
        public string Msg { get; set; }
        public DateTime Created { get; set; }

        public LogInformation() {  }
 

    }
}
