﻿using System.Collections.Generic;
using LoggerService;

namespace ChargingSystemPortalLocalization.Logging
{
    // Enum types for logger use
    public enum ServiceTypes:int
    {
        TotalConsumptionInfo = 1, ProviderInformation = 2, TotalConsumption = 3,
        GetLocalization = 4, PutLocalization = 5, Delete = 6, DailyConsumptionHourly =7, WeekConsumptionDaily=8,
        YearConsumptionMonthly=9, StationInformation= 10, ViewsReport=11, PutProvider=12, PostProvider=13,
        PutStation=14, GetUsersInfo = 15, PostUser=16, PutUser=17, GetPermissions=18, GetMenuCategories = 19, GetAccessGroupsCategories=20
    };

    // Logging user identicator
    public enum PortalUserID { UserID = 2 };

    // Cultur codes identicator
    public enum CultureCodes : int { English = 1 , Hebrew= 2, Arabic= 3, Russian= 4 };

    // ActionLogInformation - this class centralize all logging info needed for logging
    // api actions flows (entering/exiting/erorrs/status codes)
    // Uses logger's contruct base class IActionLogInformation
    public class ActionLogInformation : IActionLogInformation
    {
        // Init dicionary with all logging info
        private Dictionary<string, ActionLogInfo> _parameters =>
          new Dictionary<string, ActionLogInfo>{
            //{ "TotalConsumption (ChargingSystemPortalLocalization)",new ActionLogInfo(){ServiceType= (int)ServiceTypes.TotalConsumption , PreActionMsg = "Entering TotalConsumption Call ",PostActionMsg="Exiting TotalConsumption Call" , ActionErrorMsg="Error on TotalConsumption " } } ,
            //{ "ProviderInformation (ChargingSystemPortalLocalization)",new ActionLogInfo(){ServiceType= (int)ServiceTypes.ProviderInformation , PreActionMsg = "Entering ProviderInformation Call ",PostActionMsg="Exiting ProviderInformation Call" , ActionErrorMsg="Error on ProviderInformation " } } ,
            //{ "TotalConsumptionInfo (ChargingSystemPortalLocalization)", new ActionLogInfo() { ServiceType = (int)ServiceTypes.TotalConsumptionInfo, PreActionMsg = "Entering TotalConsumptionInfo Call with stationID = ", PostActionMsg = "After TotalConsumptionInfo Call ",  ActionErrorMsg="Error on TotalConsumptionInfo "} },
            //{ "logUserActivity (ChargingSystemPortal)", new ActionLogInfo() {  ServiceType = (int)ServiceTypes.ClickOnStation, PreActionMsg = "Entering ClickOnStation Call with userActivity = ", PostActionMsg = "After ClickOnStation Call with userActivity = ", ActionErrorMsg="Error on ClickOnStation with userActivity = " , ActionParams= true } },
            { "GetLocalization (ChargingSystemPortalLocalization)",new ActionLogInfo() { ServiceType = (int)ServiceTypes.GetLocalization , PreActionMsg = "Entering GetLocalization Call ",PostActionMsg = "Exiting GetLocalization Call" , ActionErrorMsg = "Error on GetLocalization " } },
            { "PutLocalization (ChargingSystemPortalLocalization)",new ActionLogInfo() { ServiceType = (int)ServiceTypes.PutLocalization , PreActionMsg = "Entering PutLocalization Call ",PostActionMsg = "Exiting PutLocalization Call" , ActionErrorMsg = "Error on PutLocalization " } },
            { "Delete (ChargingSystemPortalLocalization)",new ActionLogInfo() { ServiceType = (int)ServiceTypes.Delete , PreActionMsg = "Entering Delete Call ",PostActionMsg = "Exiting Delete Call" , ActionErrorMsg = "Error on Delete " } },
          //  { "DailyConsumptionHourly (ChargingSystemPortalLocalization)",new ActionLogInfo(){ServiceType= (int)ServiceTypes.DailyConsumptionHourly , PreActionMsg = "Entering DailyConsumptionHourly Call ",PostActionMsg="Exiting DailyConsumptionHourly Call" , ActionErrorMsg="Error on DailyConsumptionHourly " } } ,
           // { "WeekConsumptionDaily (ChargingSystemPortalLocalization)",new ActionLogInfo(){ServiceType= (int)ServiceTypes.WeekConsumptionDaily , PreActionMsg = "Entering WeekConsumptionDaily Call ",PostActionMsg="Exiting WeekConsumptionDaily Call" , ActionErrorMsg="Error on WeekConsumptionDaily " } } ,
           // { "YearConsumptionMonthly (ChargingSystemPortalDashboard)",new ActionLogInfo(){ServiceType= (int)ServiceTypes.YearConsumptionMonthly , PreActionMsg = "Entering YearConsumptionMonthly Call ",PostActionMsg="Exiting YearConsumptionMonthly Call" , ActionErrorMsg="Error on YearConsumptionMonthly " } } ,
           // { "StationInformation (ChargingSystemPortalDashboard)",new ActionLogInfo(){ServiceType= (int)ServiceTypes.StationInformation , PreActionMsg = "Entering StationInformation Call ",PostActionMsg="Exiting StationInformation Call" , ActionErrorMsg="Error on StationInformation " } } ,
           // { "ViewsReport (ChargingSystemPortalDashboard)",new ActionLogInfo(){ServiceType= (int)ServiceTypes.ViewsReport , PreActionMsg = "Entering ViewsReport Call ",PostActionMsg="Exiting ViewsReport Call" , ActionErrorMsg="Error on ViewsReport " } } ,
           //{ "PutProvider (ChargingSystemPortalDashboard)",new ActionLogInfo(){ServiceType= (int)ServiceTypes.PutProvider , PreActionMsg = "Entering PutProvider Call ",PostActionMsg="Exiting PutProvider Call" , ActionErrorMsg="Error on PutProvider " } } ,
           //{ "PostProvider (ChargingSystemPortalDashboard)",new ActionLogInfo(){ServiceType= (int)ServiceTypes.PostProvider , PreActionMsg = "Entering PostProvider Call ",PostActionMsg="Exiting PostProvider Call" , ActionErrorMsg="Error on PostProvider " } } ,
           //{ "PutStation (ChargingSystemPortalDashboard)",new ActionLogInfo(){ServiceType= (int)ServiceTypes.PutStation , PreActionMsg = "Entering PutStation Call ",PostActionMsg="Exiting PutStation Call" , ActionErrorMsg="Error on PutStation " } } ,
           //{ "GetUsersInfo (ChargingSystemPortalDashboard)",new ActionLogInfo(){ServiceType= (int)ServiceTypes.GetUsersInfo , PreActionMsg = "Entering GetUsersInfo Call ",PostActionMsg="Exiting GetUsersInfo Call" , ActionErrorMsg="Error on GetUsersInfo " } } ,
           //{ "PostUser (ChargingSystemPortalDashboard)",new ActionLogInfo(){ServiceType= (int)ServiceTypes.PostUser , PreActionMsg = "Entering PostUser Call ",PostActionMsg="Exiting PostUser Call" , ActionErrorMsg="Error on PostUser " } } ,
          // { "PutUser (ChargingSystemPortalDashboard)",new ActionLogInfo(){ServiceType= (int)ServiceTypes.PutUser , PreActionMsg = "Entering PutUser Call ",PostActionMsg="Exiting PutUser Call" , ActionErrorMsg="Error on PutUser " } } ,
           //{ "GetPermissions (ChargingSystemPortalDashboard)",new ActionLogInfo(){ServiceType= (int)ServiceTypes.GetPermissions , PreActionMsg = "Entering GetPermissions Call ",PostActionMsg="Exiting GetPermissions Call" , ActionErrorMsg="Error on GetPermissions " } } ,
           //{ "GetMenuCategories (ChargingSystemPortalDashboard)",new ActionLogInfo(){ServiceType= (int)ServiceTypes.GetMenuCategories , PreActionMsg = "Entering GetMenuCategories Call ",PostActionMsg="Exiting GetMenuCategories Call" , ActionErrorMsg="Error on GetMenuCategories " } } ,
           //{ "GetAccessGroupsCategories (ChargingSystemPortalDashboard)",new ActionLogInfo(){ServiceType= (int)ServiceTypes.GetAccessGroupsCategories , PreActionMsg = "Entering GetAccessGroupsCategories Call ",PostActionMsg="Exiting GetAccessGroupsCategories Call" , ActionErrorMsg="Error on GetAccessGroupsCategories " } } ,

          }; 
        public Dictionary<string, ActionLogInfo> GetLogDictonary()
        {
            return _parameters;
        }

        public int GetUserID()
        {
            return (int)PortalUserID.UserID;
        }
    }
}
