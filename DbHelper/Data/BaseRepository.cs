﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Data;
using DBHelper.Data.Models;
using Microsoft.Extensions.Logging;


namespace DBHelper.Data
{
    // BaseRepository - this class centralize and handle all db/dapper specific implementations for general use   
    public class BaseRepository
    {
          // Vars for Connection string use
        private readonly string _connectionString;
        private readonly string _password;
 

         // BaseRepository constructor - Init configuration requerments 
        public BaseRepository(IConfiguration configuration)
        {
            // Injecting configuration for connection string
            _connectionString = Environment.GetEnvironmentVariable("connPath", EnvironmentVariableTarget.Machine); 
            _password = configuration["ConnectionStrings:ChargingKey"];

            if (String.IsNullOrEmpty(_connectionString))
                throw new ArgumentException("The connection string cannot be empty.", "connectionString");
        }

    
  
        protected T QueryFirstOrDefault<T>(string sql, object parameters = null)
        {
            // Set safe connection
            using (var connection = CreateConnection())
            {
                
                return connection.QueryFirstOrDefault<T>(sql, parameters);
            }
        }

        protected int Excute(string sql, object parameters = null)
        {
            // Set safe connection
            using (var connection = CreateConnection())
            {

                return connection.Execute(sql, parameters);
            }
        }

        protected List<T> Query<T>(string sql, object parameters = null)
        {
            // Set safe connection
            using (var connection = CreateConnection())
            {
                return connection.Query<T>(sql, parameters).ToList();
            }
        }

        protected async Task<IEnumerable<T>> AsyncQuery<T>(string sql, object parameters = null)
        {
            
            // Set safe connection
            using (var connection = await CreateAsyncConnection())
            {
               
                return await connection.QueryAsync<T>(sql, parameters);
            }
        }

        protected async Task<T> AsyncQueryFirstOrDefault<T>(string sql, object parameters = null)
        {

            // Set safe connection
            using (var connection = await CreateAsyncConnection())
            {

                return await connection.QueryFirstOrDefaultAsync<T>(sql, parameters);
            }
        }


        // This(home made..) method returns a generic IEnumerable based on two generic hirarchial classes driven from IMultiMapper(home made class..) base class
        // All any ather DTO/Models that are going to use the BaseRepository class with multi mapping requerments must be driven from IMultiMapper class 
        protected async Task<IEnumerable<T1>> QueryMulti<T1, T2, TResult>(string strQuery, string splitOnField, DynamicParameters parameters=null)
            where T1 : IMultiMapper, new()
            where T2 : IMultiMapper, new()
        {

            // Set safe Async connection
            using (var connection = await CreateAsyncConnection())
            {
                Dictionary<int, T1> queryDictionary = new Dictionary<int, T1>();
                   
                return await connection.QueryAsync(strQuery, delegate (T1 c , T2 o)
                 {
                    if (!queryDictionary.TryGetValue(c.GetID(), out var value))
                    {
                        value = c;
                        //value.provider = new ProviderSummeryGetManyResponse();
                        queryDictionary.Add(value.GetID(), value);
                    }
                    if (o != null)
                    {
                         value.SetChild(o);
                     }
                     
                     return (value);

                }, parameters, null, buffered: true, splitOnField);
            }

        }

  
    
    protected int Execute(string sql, object parameters = null)
        {
            int result= 0;
            // Set safe connection
            using (var connection = CreateConnection())
            {
                try
                {
                    result =  connection.Execute(sql, parameters);
                }
                catch (Exception ex)
                {
                    throw new ArgumentException("Error in BaseRepository.Execute() with params - ", sql + parameters.ToString());
                }

                return result;
            }
        }


        // Create/Open connection
        protected IDbConnection CreateConnection()
        {
             var connection = new  SqlConnection(_connectionString);
            try
            {
                 connection.Open();
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Error in BaseRepository.CreateConnection() with params - ", _connectionString);
            }
            // initialize connection .
            return connection;
        }

        // Create/Open Async connection
        protected async Task<IDbConnection> CreateAsyncConnection()
        {
            var connection = new SqlConnection(_connectionString);
            try
            {
                await connection.OpenAsync();
            }
            catch(Exception ex)
            {
                throw new ArgumentException("Error in BaseRepository.CreateAsyncConnection() with params - " , _connectionString);
            }
            // initialize connection .
            return connection;
        }


    }
}
