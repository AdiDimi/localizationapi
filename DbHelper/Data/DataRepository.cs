﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DAL.Data.Models;
using System.Data;

namespace DAL.Data
{
    // DataRepository - this class centralize all DAL methodes and uses base class IDataRepository to handle db/dapper specification   
    public class DataRepository : BaseRepository, IDataRepository
    {
        // DataRepository constractor - Sends configuration requerments to base class
        public DataRepository(IConfiguration configuration) : base(configuration) { }

        public bool ProviderExists(int customerid)
        {
            throw new NotImplementedException();
        }

        // This method returns a summeriesd info list of stations and there providers
        public async Task<IEnumerable<StationSummeryGetManyResponse>> GetAsyncAllStationsSummered()
        {
     
                return(await  QueryMulti<StationSummeryGetManyResponse, ProviderSummeryGetManyResponse, StationSummeryGetManyResponse>
                      (SELECT_All_STATIONS_SUMMERED, "ProviderName"));
       
        }

        // This method returns a extended list of info on stations and there connectors
        public async Task<IEnumerable<StationExtendedGetSingleResponse>> GetAsynStationExtendedInfo(int stationID)
        {
            var parameters = new DynamicParameters();
            parameters.Add(name: "@StationID", stationID, dbType: DbType.Int32, direction: ParameterDirection.Input);

            return (await QueryMulti<StationExtendedGetSingleResponse, StationConnectorExtandedGetManyResponse, StationExtendedGetSingleResponse>
                     (SELECT_STATION_EXTENDED_INFO, "Postcode", parameters));

   
        }


    }
}

