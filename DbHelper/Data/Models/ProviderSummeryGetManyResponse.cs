﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DAL.Data.Models
{
    public class ProviderSummeryGetManyResponse:BaseMultiMapper
    {
        [Required]
        public int ID { get; set; }

        [Column(TypeName = "varchar (50)"), Required]
        public string ProviderName { get; set; }

        [Required]
        public int ProviderType { get; set; }

        public override int GetID()
        {
            return ID;
        }

        public override void SetChild(BaseMultiMapper childObject)
        {
           // throw new NotImplementedException();
        }
    }
}
