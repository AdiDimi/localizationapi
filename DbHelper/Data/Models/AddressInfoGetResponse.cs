﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Data.Models
{
    public class AddressInfoGetResponse
    {
        [Required]
        public int ID { get; set; }

        [Column(TypeName = "varchar (50)"), Required]
        public string StateName { get; set; }

        [Column(TypeName = "varchar (50)"), Required]
        public string CityName { get; set; }

        [Column(TypeName = "varchar (9)"), Required]
        public string Postcode { get; set; }

        [Required]
        public float Lng { get; set; }
        [Required]
        public float Lat { get; set; }
    }
}
