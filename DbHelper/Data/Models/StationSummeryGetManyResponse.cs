﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DAL.Data.Models
{
    public class StationSummeryGetManyResponse:BaseMultiMapper
    {
        [Required]
        public int ID { get; set; }

        [Column(TypeName = "varchar (50)"), Required]
        public string Name { get; set; }

        [StringLength(50)]
        public string StreetName { get; set; }

        [Required]
        public double Lng { get; set; }
        [Required]
        public double Lat { get; set; }

        [Required]
        public int Status { get; set; }

        public ProviderSummeryGetManyResponse provider { get; set; }

        public override int GetID()
        {
            return ID;
        }

        public override void SetChild(BaseMultiMapper childObject)
        {
            provider = (ProviderSummeryGetManyResponse)childObject;
        }

    }
}
