﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace DAL.Data.Models
{
    public class StationConnectorExtandedGetManyResponse:BaseMultiMapper
    {
        [Column("RefID"),Required]
        public int? RefId { get; set; }

        [Required]
        public int ConnectorType { get; set; }
        
        [Column(TypeName = "money")]
        public decimal Power { get; set; }
        public int PriceType { get; set; }
        [Column(TypeName = "money")]
        public decimal Price { get; set; }
        public bool IncludingVat { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public int DaysOfWeek { get; set; }
        public bool Reservation { get; set; }

        public override int GetID()
        {
            return (int)RefId;
        }

        public override void SetChild(BaseMultiMapper childObject)
        {
           // throw new NotImplementedException();
        }

  
    }
}
