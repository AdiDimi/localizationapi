﻿

namespace DBHelper.Data.Models
{
    // IMultiMapper - this inteface enables driven classes implementing generic multy mapping 
    public interface  IMultiMapper
    {
         public int GetID();
         public void SetChild(IMultiMapper childObject);
       
    }
}
