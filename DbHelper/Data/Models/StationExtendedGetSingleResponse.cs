﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Data.Models
{
    public class StationExtendedGetSingleResponse:BaseMultiMapper
    {
        [Required]
        public int ID { get; set; }

        [Column(TypeName = "varchar (50)"), Required]
        public string ProviderName { get; set; }

        [Required]
        public int ProviderType { get; set; }

        [Required]
        public Guid RefId { get; set; }
       
        [StringLength(50)]
        public string Name { get; set; }
        public int Status { get; set; }
       
        [StringLength(50)]
        public string StreetName { get; set; }

        [Required]
        public double Lng { get; set; }
        [Required]
        public double Lat { get; set; }

        [Column(TypeName = "money")]
        public decimal MaxPower { get; set; }
        public int VisibilityScope { get; set; }

        public  List<StationConnectorExtandedGetManyResponse> StationConnectors { get; set; }

        public override int GetID()
        {
            return ID;
        }

        public override void SetChild(BaseMultiMapper childObject)
        {
            if (StationConnectors == null)
                StationConnectors = new List<StationConnectorExtandedGetManyResponse>();
            StationConnectors.Add((StationConnectorExtandedGetManyResponse)childObject);
        }
    }
}
