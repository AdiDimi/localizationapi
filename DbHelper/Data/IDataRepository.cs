﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Data.Models;

namespace DAL.Data
{
    public interface IDataRepository
    {
        #region ExtantionOptions
        //IEnumerable<Customer> GetCustomers();

        //IEnumerable<Product> GetProductsBySearch(string search);

        //IEnumerable<Customer> GetCustomersWithOrders();

        //Customer GetCustomer(int customerid);

        //Task<Customer> GetAsyncCustomer(int customerId);

        //bool CustomerExists(int customerid); 
        #endregion

        Task<IEnumerable<StationSummeryGetManyResponse>> GetAsyncAllStationsSummered();
        Task<IEnumerable<StationExtendedGetSingleResponse>> GetAsynStationExtendedInfo(int stationID); 

    }
}
