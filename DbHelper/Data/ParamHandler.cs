﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace DBHelper.Data  //delete if not in use 
{
    public class ParamHandler
    {
        private readonly SortedDictionary<string, object> _parameters
                                    = new SortedDictionary<string, object>();
        private readonly DynamicParameters parms = new DynamicParameters();
       
        public DynamicParameters Parameters
        {
            get
            {
                var parms = new DynamicParameters();
                foreach (var parameterName in _parameters.Keys)
                {
                    parms.Add(parameterName, _parameters[parameterName]);
                }
                return parms;
            }
        }

        protected string BuildSqlWhereStatmentByParams
        {
            get
            {
                if (_parameters.Keys.Count == 0)
                    throw new Exception("Attempted to perform an insert without any input parameters.");

                //Create a comma-separated list for 
                //the WHERE part of the clause
                string whereFields = string.Join($"AND {_parameters.Keys}=@", _parameters.Keys);
                whereFields = whereFields.Remove(0, 3);


                return " WHERE " + whereFields;

            }
        }

        public ParamHandler Add<T>(string parameterName, T value)
        {
            if (_parameters.ContainsKey(parameterName))
                throw new DuplicateNameException("This field was already declared");
            switch (Type.GetTypeCode(value.GetType()))
            {
                case TypeCode.Int32:
                    parms.Add(name: parameterName, parameterName, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    break;
                case TypeCode.Decimal:
                    parms.Add(name: "@StationID", parameterName, dbType: DbType.Decimal, direction: ParameterDirection.Input);
                    break;

            }
            DbType dbType =  (value.GetType() == typeof(Int32)) ? DbType.Int32 : DbType.Int32;
            _parameters.Add(parameterName, value);
            
            return this;
        }

    }
}
