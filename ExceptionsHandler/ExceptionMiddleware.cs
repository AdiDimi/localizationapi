﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using LoggerService;
using System.Net;
using ExceptionsHandler.Models;
using Microsoft.Extensions.Logging;

namespace ExceptionsHandler
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILoggerManager _logger;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _logger = new LoggerManager();
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {

                Exception exception = (ex.InnerException == null) ? ex : ex.InnerException;
                if (exception.Data.Count == 0)
                    exception.Data["LogInfo"] = new LogInformation { UserID = 1, Created = DateTime.Now, LevelEx = (int)LogLevel.Error, ServiceType = 1, Msg = exception.Message };
                _logger.LogError(exception.Message + " {@LogError}", (LogInformation)exception.Data["LogInfo"]);
                await HandleExceptionAsync(httpContext, exception);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            var message = exception switch
            {
                AccessViolationException => "Internal Server Error from the custom middleware",
                _ => "Internal Server Error."
            };

            await context.Response.WriteAsync(new ErrorDetails()
            {
                StatusCode = context.Response.StatusCode,
                Message = message
            }.ToString());
        }

    }
}
