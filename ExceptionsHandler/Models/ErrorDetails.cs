﻿
using System;
using System.Text.Json;

namespace ExceptionsHandler.Models
{
    public class ErrorDetails
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }


        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }

    public class LogInformation
    {

        public int? UserID { get; set; }
        public int ServiceType { get; set; }
        public int LevelEx { get; set; }
        public string Msg { get; set; }
        public DateTime Created { get; set; }

        public LogInformation() { }


    }
}