﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace LoggerService
{
    public interface ILoggerManager : ILogger
    {
     
        void LogInformation(string message, params object[] logArgs);
        void LogWarn(string message, params object[] logArgs);
        void LogDebug(string message, params object[] logArgs);
        void LogError(string message, params object[] logArgs);
        public void Flush();

    }
}
