﻿
using System;

// See SLF (System Logging Facade) for .NET, should probably already do this; chaval for you to reinvent the wheel

namespace LoggerService
{
    // LoggerManager - Logging rapper class,
    // incapsulate driven ILoggerManager loggers specific implemntations   
    public class LoggerManager : ILoggerManager
    {
        // local vars init
        private static ILoggerManager logger;
        
        // Enum definition of logger types
        public enum LoggerTypes { NLog=1,Serrilog=2, console=3};
        
        // Init current logger type 
        public static LoggerTypes currLoggerType = LoggerTypes.NLog;

        public LoggerManager() 
        {
            // Singelton local logger var init
            if(LoggerManager.logger == null)   
                switch(currLoggerType)
                {
                    case LoggerTypes.NLog:
                        logger = new NLogLogger();
                        break;
                    case LoggerTypes.Serrilog:
                        break;
                    case LoggerTypes.console:
                        break;
                }
            
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public void LogDebug(string message, params object[] logArgs)
        {
              logger.LogDebug(message, logArgs);
        }

        public void LogError(string message, params object[] logArgs)
        {
            logger.LogError(message, logArgs);
        }

        public void LogInformation(string message, params object[] logArgs)
        {
             logger.LogInformation(message, logArgs);
        }

        public void LogWarn(string message, params object[] logArgs)
        {
            logger.LogWarn(message, logArgs);
        }

        public void Flush()
        {
            logger.Flush();
        }
    
        public void Log<TState>(Microsoft.Extensions.Logging.LogLevel logLevel, 
            Microsoft.Extensions.Logging.EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {

            logger.LogInformation($"Level: {logLevel}, Event ID: {eventId.Id}");

            if (state != null)
            {
                logger.LogInformation(($", State: {state}"));
            }

            if (exception != null)
            {
                logger.LogInformation(($", Exeption: {exception.Message}"));
            }


        }

        public bool IsEnabled(Microsoft.Extensions.Logging.LogLevel logLevel)
        {
            throw new NotImplementedException();
        }
    }
}
