﻿using NLog;
using System;
using NLog.Web;

namespace LoggerService
{
    public class NLogLogger : ILoggerManager
    {
        //private static ILogger logger = LogManager.GetCurrentClassLogger();
        private static ILogger logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public void LogDebug(string message, params object[] logArgs)
        {
              logger.Debug(message, logArgs);
        }

        public void LogError(string message, params object[] logArgs)
        {
            logger.Error(message, logArgs);
        }

        public void LogInformation(string message, params object[] logArgs)
        {
             logger.Info(message, logArgs);
        }

        public void LogWarn(string message, params object[] logArgs)
        {
            logger.Warn(message, logArgs);
        }

    
        public void Log<TState>(Microsoft.Extensions.Logging.LogLevel logLevel, Microsoft.Extensions.Logging.EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {

            logger.Info($"Level: {logLevel}, Event ID: {eventId.Id}");

            if (state != null)
            {
                logger.Info(($", State: {state}"));
            }

            if (exception != null)
            {
                logger.Info(($", Exeption: {exception.Message}"));
            }


        }

        public bool IsEnabled(Microsoft.Extensions.Logging.LogLevel logLevel)
        {
            throw new NotImplementedException();
        }

        public void Flush()
        {
            LogManager.Shutdown(); 
        }
    }
}
