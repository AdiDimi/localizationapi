﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LoggerService
{
    public class AutoLogMiddleWare
    {
        private readonly RequestDelegate _next;

        public AutoLogMiddleWare(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                var controllerActionDescriptor = context.Request.Path;
      //  .Metadata
        

                //var controllerName = controllerActionDescriptor.ControllerName;
               // var actionName = controllerActionDescriptor.ActionName;

                //LogInformation info = new LogInformation(context);
                
                await _next(context);
                //string httpStatus = "0";

                //// Log Request
                //var originalRequestBody = context.Request.Body;
                //originalRequestBody.Seek(0, SeekOrigin.Begin);
                //string requestBody = new StreamReader(originalRequestBody).ReadToEnd();
                //originalRequestBody.Seek(0, SeekOrigin.Begin);

                //// Log Response
                //string responseBody = string.Empty;
                //using (var swapStream = new MemoryStream())
                //{

                //    var originalResponseBody = context.Response.Body;
                //    context.Response.Body = swapStream;
                //    await _next(context);
                //    swapStream.Seek(0, SeekOrigin.Begin);
                //    responseBody = new StreamReader(swapStream).ReadToEnd();
                //    swapStream.Seek(0, SeekOrigin.Begin);
                //    await swapStream.CopyToAsync(originalResponseBody);
                //    context.Response.Body = originalResponseBody;
                //    httpStatus = context.Response.StatusCode.ToString();
                //}

                //// Clean route
                //string cleanRoute = route;
                //foreach (var c in Path.GetInvalidFileNameChars())
                //{
                //    cleanRoute = cleanRoute.Replace(c, '-');
                //}

                //StringBuilder sbRequestHeaders = new StringBuilder();
                //foreach (var item in context.Request.Headers)
                //{
                //    sbRequestHeaders.AppendLine(item.Key + ": " + item.Value.ToString());
                //}

                //StringBuilder sbResponseHeaders = new StringBuilder();
                //foreach (var item in context.Response.Headers)
                //{
                //    sbResponseHeaders.AppendLine(item.Key + ": " + item.Value.ToString());
                //}


                //string filename = DateTime.Now.ToString("yyyyMMdd.HHmmss.fff") + "_" + httpStatus + "_" + cleanRoute + ".log";
                //StringBuilder sbLog = new StringBuilder();
                //sbLog.AppendLine("Status: " + httpStatus + " - Route: " + route);
                //sbLog.AppendLine("=============");
                //sbLog.AppendLine("Request Headers:");
                //sbLog.AppendLine(sbRequestHeaders.ToString());
                //sbLog.AppendLine("=============");
                //sbLog.AppendLine("Request Body:");
                //sbLog.AppendLine(requestBody);
                //sbLog.AppendLine("=============");
                //sbLog.AppendLine("Response Headers:");
                //sbLog.AppendLine(sbResponseHeaders.ToString());
                //sbLog.AppendLine("=============");
                //sbLog.AppendLine("Response Body:");
                //sbLog.AppendLine(responseBody);
                //sbLog.AppendLine("=============");

                //var path = Directory.GetCurrentDirectory();
                //string filepath = ($"{path}\\Logs\\{filename}");
                //File.WriteAllText(filepath, sbLog.ToString());
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }

        }
    }

    //public class EnableRequestRewindMiddleware
    //{
    //    private readonly RequestDelegate _next;

    //    public EnableRequestRewindMiddleware(RequestDelegate next)
    //    {
    //        _next = next;
    //    }

    //    public async Task Invoke(HttpContext context)
    //    {
    //        //context.Request.EnableRewind();
    //        await _next(context);
    //    }
    //}

    
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseAutoLogMiddleWare<AutoLogMiddleWare>(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AutoLogMiddleWare>();
        }
    }
}
